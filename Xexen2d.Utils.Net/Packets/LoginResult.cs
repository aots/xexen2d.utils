﻿using LiteNetLib.Utils;

namespace Xexen2d.Utils.Packets
{
    public class LoginResult : ASyncable
    {
        public bool Yourself { get; set; }

        public override ASyncable Clone()
        {
            return new LoginResult { ID = ID, Yourself = Yourself };
        }

        public override bool Equals(ASyncable other)
        {
            var item = other as LoginResult;
            if (item == null)
                return false;

            if (item.ID == item.ID && item.Yourself == Yourself)
                return true;

            return false;
        }

        public override void Serialize(NetDataWriter writer, NetSerializer serializer)
        {
            serializer.Serialize(writer, this);
        }
    }
}