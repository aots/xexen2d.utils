﻿using LiteNetLib.Utils;
using System;

namespace Xexen2d.Utils.Packets
{
    public class PlayerShotPacket : ASyncable
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Angle { get; set; }

        public override ASyncable Clone()
        {
            return new PlayerShotPacket()
            {
                ID = ID,
                X = X,
                Y = Y,
                Angle = Angle
            };
        }

        public override bool Equals(ASyncable other)
        {
            var item = other as PlayerShotPacket;
            if (item == null)
                return false;

            if (item.ID == ID && item.X == X && item.Y == Y && item.Angle == Angle)
                return true;

            return false;
        }

        public override void Serialize(NetDataWriter writer, NetSerializer serializer)
        {
            serializer.Serialize(writer, this);
        }
    }
}