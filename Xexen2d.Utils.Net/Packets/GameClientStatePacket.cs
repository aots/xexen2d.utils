﻿using LiteNetLib.Utils;

namespace Xexen2d.Utils.Packets
{
    public class GameClientStatePacket : ASyncable
    {
        public bool IsExiting { get; set; }

        public override ASyncable Clone()
        {
            return new GameClientStatePacket()
            {
                ID = ID,
                IsExiting = IsExiting
            };
        }

        public override bool Equals(ASyncable other)
        {
            var item = other as GameClientStatePacket;
            if (item == null)
                return false;

            if (item.IsExiting == IsExiting && item.ID == ID)
                return true;

            return false;
        }

        public override void Serialize(NetDataWriter writer, NetSerializer serializer)
        {
            serializer.Serialize(writer, this);
        }
    }
}