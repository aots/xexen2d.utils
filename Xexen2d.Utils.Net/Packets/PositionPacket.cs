﻿using LiteNetLib.Utils;

namespace Xexen2d.Utils.Packets
{
    public class PositionPacket : ASyncable
    {
        public float X { get; set; }
        public float Y { get; set; }

        public override ASyncable Clone()
        {
            return new PositionPacket()
            {
                ID = ID,
                X = X,
                Y = Y
            };
        }

        public override bool Equals(ASyncable other)
        {
            var item = other as PositionPacket;
            if (item == null)
                return false;

            if (item.ID == ID && item.X == X && item.Y == Y)
                return true;

            return false;
        }

        public override void Serialize(NetDataWriter writer, NetSerializer serializer)
        {
            serializer.Serialize(writer, this);
        }
    }
}