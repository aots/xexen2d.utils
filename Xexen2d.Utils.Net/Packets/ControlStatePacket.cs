﻿using LiteNetLib.Utils;

namespace Xexen2d.Utils.Packets
{
    public class ControlStatePacket : ASyncable
    {
        public EControlState ControlState { get; set; } = EControlState.Idle;
        public bool Jumping { get; set; } = false;
        public bool Shooting { get; set; } = false;

        public override ASyncable Clone()
        {
            return new ControlStatePacket()
            {
                ControlState = ControlState,
                Jumping = Jumping,
                ID = ID,
                Shooting = Shooting
            };
        }

        public override bool Equals(ASyncable other)
        {
            var item = other as ControlStatePacket;
            if (item == null)
                return false;

            if (item.ID == ID && ControlState == item.ControlState && Jumping == item.Jumping && Shooting == item.Shooting)
                return true;

            return false;
        }

        public override void Serialize(NetDataWriter writer, NetSerializer serializer)
        {
            serializer.Serialize(writer, this);
        }
    }
}