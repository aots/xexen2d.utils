﻿using LiteNetLib.Utils;
using System;

namespace Xexen2d.Utils.Packets
{
    public interface INetSyncable : ICloneable
    {
        void Serialize(NetDataWriter writer, NetSerializer serializer);

        new INetSyncable Clone();
    }
}