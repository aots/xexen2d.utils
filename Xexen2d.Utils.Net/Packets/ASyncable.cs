﻿using System;
using LiteNetLib.Utils;

namespace Xexen2d.Utils.Packets
{
    public abstract class ASyncable : INetSyncable, IEquatable<ASyncable>
    {
        public virtual long ID { get; set; }

        public abstract bool Equals(ASyncable other);

        public abstract ASyncable Clone();

        public abstract void Serialize(NetDataWriter writer, NetSerializer serializer);

        INetSyncable INetSyncable.Clone()
        {
            return Clone();
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}