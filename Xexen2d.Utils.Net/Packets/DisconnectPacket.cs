﻿using LiteNetLib.Utils;

namespace Xexen2d.Utils.Packets
{
    public class DisconnectPacket : ASyncable
    {
        public string Message { get; set; }

        public override ASyncable Clone()
        {
            return new DisconnectPacket()
            {
                ID = ID,
                Message = Message
            };
        }

        public override bool Equals(ASyncable other)
        {
            var item = other as DisconnectPacket;
            if (item == null)
                return false;

            if (item.ID == ID && item.Message == Message)
                return true;

            return false;
        }

        public override void Serialize(NetDataWriter writer, NetSerializer serializer)
        {
            serializer.Serialize(writer, this);
        }
    }
}