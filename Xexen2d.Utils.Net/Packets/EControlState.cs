﻿namespace Xexen2d.Utils.Packets
{
    public enum EControlState : byte
    {
        Idle,
        MoveRight,
        MoveLeft
    }
}