﻿using LiteNetLib.Utils;

namespace Xexen2d.Utils.Packets
{
    public class LoginRequest : ASyncable
    {
        public override ASyncable Clone()
        {
            return new LoginRequest();
        }

        public override bool Equals(ASyncable other)
        {
            var item = other as LoginRequest;
            if (item == null)
                return false;

            if (item.ID == ID)
                return true;

            return false;
        }

        public override void Serialize(NetDataWriter writer, NetSerializer serializer)
        {
            serializer.Serialize(writer, this);
        }
    }
}